from django.contrib import admin

from applications.partners.models import Partner


class PartnerAdmin(admin.ModelAdmin):
    list_display = ['name', 'logo']


admin.site.register(Partner, PartnerAdmin)
