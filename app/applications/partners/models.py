from django.db import models


class Partner(models.Model):
    name = models.CharField(max_length=50)
    logo = models.ImageField(upload_to='images')

    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.name
