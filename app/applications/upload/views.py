import os

from django.conf import settings
from django.core.files.storage import FileSystemStorage
from django.http import JsonResponse
from django.shortcuts import render


def main_page(request):
    return render(request, 'index.html', {})


def baby_ai(request):
    if request.method == "POST" and request.FILES["image_file"]:
        image_file = request.FILES["image_file"]
        fs = FileSystemStorage()
        filename = fs.save(image_file.name, image_file)
        image_url = fs.url(filename)

        # check whether there is a person in the photo and quality is good enough
        # errors = 'There is no any person in the photo or the photo quality is poor.'
        errors = None

        if not errors:
            gender = 'man'
            probability = '95'

        return render(request, "upload.html", locals())
    return render(request, "upload.html")


def baby_ai_2_models(request):
    if request.method == "POST" and request.FILES["image_file"]:
        image_file = request.FILES["image_file"]
        fs = FileSystemStorage()
        filename = fs.save(image_file.name, image_file)
        image_url = fs.url(filename)

        # check whether there is a person in the photo and quality is good enough
        # errors = 'There is no any person in the photo or the photo quality is poor.'
        errors = None

        if not errors:
            gender = 'man'
            probability = '95'

        # set face_img_path to face capture image
        face_img_path = '/staticfiles/images/face.jpeg'

        return render(request, "upload_2.html", locals())
    return render(request, "upload_2.html")


def label_view(request):
    files = os.listdir(f'{settings.MEDIA_ROOT}/all/')
    data = {'files': files}
    return render(request, "label.html", data)


def move_image_view(request, file_name, dir_name):
    os.replace(f'{settings.MEDIA_ROOT}/all/{file_name}', f'{settings.MEDIA_ROOT}/{dir_name}/{file_name}')
    return JsonResponse({'status': '200'})


def moves_like_jagger(request):
    return render(request, 'moves_like_jagger.html', {})


def upload(request):
    if request.method == "POST" and request.FILES["image_file"]:
        image_file = request.FILES["image_file"]
        fs = FileSystemStorage()
        filename = fs.save(image_file.name, image_file)
        image_url = fs.url(filename)
        return render(request, "hsl_control.html", locals())
    return render(request, "hsl_control.html")
