# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from applications.certificates.models import Certificate
from applications.partners.models import Partner
from .models import Product
from .forms import SendEmailForm, SendEmailKGForm, SendEmailENForm
from django.views.decorators.http import require_POST
from django.shortcuts import render, get_object_or_404
from django.http import JsonResponse
from applications.products.tasks import send_user_email
import threading


def products_list_ru(request):
    products = Product.objects.all()
    partners = Partner.objects.all()
    certificates = Certificate.objects.all()
    form = SendEmailForm()
    return render(request, 'index.html', locals())


def products_list_kg(request):
    products = Product.objects.all()
    partners = Partner.objects.all()
    certificates = Certificate.objects.all()
    form = SendEmailKGForm()
    return render(request, 'index_kg.html', locals())


def products_list_en(request):
    products = Product.objects.all()
    partners = Partner.objects.all()
    certificates = Certificate.objects.all()
    form = SendEmailENForm()
    return render(request, 'index_en.html', locals())


def product_details_ru(request, product_id):
    product = get_object_or_404(Product, id=product_id)
    return render(request, 'products/details.html', {
        'product': product,
    })


def product_details_en(request, product_id):
    product = get_object_or_404(Product, id=product_id)
    return render(request, 'products/details_en.html', {
        'product': product,
    })


def product_details_kg(request, product_id):
    product = get_object_or_404(Product, id=product_id)
    return render(request, 'products/details_kg.html', {
        'product': product,
    })


@require_POST
def send_email(request):
    form = SendEmailENForm(request.POST or None)
    if form.is_valid():
        name = form.cleaned_data['name']
        email = form.cleaned_data['email']
        number = form.cleaned_data['number']
        email_message = form.cleaned_data['email_message']
        message = '{message}. {user}, {email}, {number}'.format(message=email_message, user=name, email=email, number=number)
        send_user_email.delay('Заявка', message, email)
        # thr = threading.Thread(target=send_user_email, args=(u'Заявка', message, ''))
        # thr.start()
        return JsonResponse({'response': 'Ваши данные успешно отправлены на сервер'})
    return JsonResponse({'response': 'Error', 'error': form.errors})


@require_POST
def send_email_kg(request):
    form = SendEmailKGForm(request.POST or None)
    if form.is_valid():
        name = form.cleaned_data['name']
        email = form.cleaned_data['email']
        number = form.cleaned_data['number']
        email_message = form.cleaned_data['email_message']
        message = '{message}. {user}, {email}, {number}'.format(message=email_message, user=name, email=email, number=number)
        send_user_email.delay('Заявка', message, email)
        # thr = threading.Thread(target=send_user_email, args=(u'Заявка', message, ''))
        # thr.start()
        return JsonResponse({'response': 'Ваши данные успешно отправлены на сервер'})
    return JsonResponse({'response': 'Error', 'error': form.errors})


@require_POST
def send_number(request):
    number = request.POST['number']
    if len(number) == 10:
        message = u'Посетитель сайта с номером {number} попросил Вас перезвонить ему'.format(number=number)
        thr = threading.Thread(target=send_user_email, args=(u'Заявка на звонок', message, ''))
        thr.start()

        return JsonResponse({'response': u'Ваша заявка отправлена'})
    return JsonResponse({'response': u'Error', 'error': u'Номер введен неверно'})
