from celery import shared_task
from django.conf import settings
from django.core.mail import send_mail


@shared_task
def send_user_email(title, message, email):
    send_mail(
        title,
        message,
        'devala.smtp@gmail.com',
        ['kasietmuktarbek94@mail.ru'],
        # email,
        fail_silently=False,
    )
    return True
