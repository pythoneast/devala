from django.urls import path

from applications.products import views

app_name = 'products'

urlpatterns = [
    path('', views.products_list_ru, name='product-list-ru'),
    path('kg/', views.products_list_kg, name='product-list-kg'),
    path('en/', views.products_list_en, name='product-list-en'),
    path('send_email/', views.send_email, name='send_mail'),
    path('send_number/', views.send_number, name='send_number'),
    path('products/<int:product_id>/', views.product_details_ru, name='product-details'),
    path('products/kg/<int:product_id>/', views.product_details_kg, name='product-details-kg'),
    path('products/en/<int:product_id>/', views.product_details_en, name='product-details-en'),
]
