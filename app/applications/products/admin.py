from django.contrib import admin
from django import forms
from ckeditor.widgets import CKEditorWidget

from applications.products.models import Product


class ProductAdminForm(forms.ModelForm):
    full_description_ru = forms.CharField(widget=CKEditorWidget())
    full_description_en = forms.CharField(widget=CKEditorWidget())
    full_description_kg = forms.CharField(widget=CKEditorWidget())

    class Meta:
        fields = '__all__'
        model = Product


class ProductAdmin(admin.ModelAdmin):
    list_display = ['title_ru', 'short_description_ru']
    form = ProductAdminForm


admin.site.register(Product, ProductAdmin)
