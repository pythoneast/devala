from django import forms
from .models import SendEmail


class SendEmailForm(forms.ModelForm):
    class Meta:
        model = SendEmail
        fields = '__all__'
        widgets = {
            'name': forms.TextInput(attrs={'placeholder': u'Имя', 'class': 'form-control name'}),
            'email': forms.TextInput(attrs={'placeholder': u'Email', 'class': 'form-control email'}),
            'number': forms.TextInput(attrs={'placeholder': u'Номер телефона', 'class': 'form-control number'}),
            'email_message': forms.Textarea(attrs={'placeholder': u'Сообщение', 'class': 'form-control message'}),
        }


class SendEmailKGForm(forms.ModelForm):
    class Meta:
        model = SendEmail
        fields = '__all__'
        widgets = {
            'name': forms.TextInput(attrs={'placeholder': u'Аты', 'class': 'form-control name'}),
            'email': forms.TextInput(attrs={'placeholder': u'Email', 'class': 'form-control email'}),
            'number': forms.TextInput(attrs={'placeholder': u'Телефон', 'class': 'form-control number'}),
            'email_message': forms.Textarea(attrs={'placeholder': u'Текст', 'class': 'form-control message'}),
        }


class SendEmailENForm(forms.ModelForm):
    class Meta:
        model = SendEmail
        fields = '__all__'
        widgets = {
            'name': forms.TextInput(attrs={'placeholder': u'Name', 'class': 'form-control name'}),
            'email': forms.TextInput(attrs={'placeholder': u'Email', 'class': 'form-control email'}),
            'number': forms.TextInput(attrs={'placeholder': u'Number', 'class': 'form-control number'}),
            'email_message': forms.Textarea(attrs={'placeholder': u'Message', 'class': 'form-control message'}),
        }
