from django.db import models
from django.urls import reverse


class Product(models.Model):
    title_ru = models.CharField(max_length=50, default='qwerty')
    title_en = models.CharField(max_length=50, default='qwerty')
    title_kg = models.CharField(max_length=50, default='qwerty')
    short_description_ru = models.TextField(max_length=100, default='qwerty')
    short_description_kg = models.TextField(max_length=100, default='qwerty')
    short_description_en = models.TextField(max_length=100, default='qwerty')
    full_description_ru = models.TextField(default='qwerty')
    full_description_en = models.TextField(default='qwerty')
    full_description_kg = models.TextField(default='qwerty')
    image = models.ImageField(upload_to='images')

    def __unicode__(self):
        return self.title_ru + self.title_en + self.title_kg

    def __str__(self):
        return self.title_ru + self.title_en + self.title_kg

    def get_absolute_url(self):
        return reverse('product:product-details', args=[self.id])


class SendEmail(models.Model):
    name = models.CharField(max_length=20)
    email = models.EmailField(max_length=50, blank=True)
    number = models.CharField(max_length=20)
    email_message = models.TextField(blank=True)
    created = models.DateTimeField(auto_now=True)
