from django.db import models


class Certificate(models.Model):
    name = models.CharField(max_length=25)
    image = models.ImageField(upload_to='media', blank=True, null=True)

    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.name
