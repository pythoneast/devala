from django.contrib import admin
from applications.certificates.models import Certificate


admin.site.register(Certificate)
