<hr>
<p>This is a website of DEVALA.KG</p>

<b>Development Version</b>

<ol>
  <li>Copy .env-example file and paste as a .env file</li>
  <li>Change direction to the folder with docker-compose.yml file</li>
  <li>Run the command: docker-compose up -d --build</li>
  <li>Browse to one of the following links: http://127.0.0.1:8000 http://localhost:8000</li>
</ol>
